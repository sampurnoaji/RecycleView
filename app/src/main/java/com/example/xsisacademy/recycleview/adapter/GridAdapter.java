package com.example.xsisacademy.recycleview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.xsisacademy.recycleview.R;
import com.example.xsisacademy.recycleview.data.Presiden;

import java.util.ArrayList;

/**
 * Created by XsisAcademy on 24/06/2019.
 */

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder>{

    private Context context;
    private ArrayList<Presiden> listPresiden;

    public GridAdapter(Context context, ArrayList<Presiden> listPresiden) {
        this.context = context;
        this.listPresiden = listPresiden;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<Presiden> getListPresiden() {
        return listPresiden;
    }

    public void setListPresiden(ArrayList<Presiden> listPresiden) {
        this.listPresiden = listPresiden;
    }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_presiden_grid, parent, false);
        return new GridViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewHolder holder, int position) {
        final Presiden p = listPresiden.get(position);
        Glide.with(context)
                .load(p.getImg())
                .into(holder.imgPhoto);
    }

    @Override
    public int getItemCount() {
        return listPresiden.size();
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        public GridViewHolder(View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.imgItemPhoto);
        }
    }
}
