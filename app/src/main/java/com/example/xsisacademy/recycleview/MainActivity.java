package com.example.xsisacademy.recycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.example.xsisacademy.recycleview.adapter.CardViewAdapter;
import com.example.xsisacademy.recycleview.adapter.GridAdapter;
import com.example.xsisacademy.recycleview.data.Presiden;
import com.example.xsisacademy.recycleview.data.PresidenData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView itemCategory;
    private ArrayList<Presiden> data;
    private CardViewAdapter cardViewAdapter;
    private GridAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("RecyclerView - CardView");

        setupView();
        populateDummyData();
        showRecyclerCardView();
    }

    private void setupView() {
        itemCategory = findViewById(R.id.itemCategory);
    }

    private void populateDummyData() {
        data = new ArrayList<>();
        data.addAll(PresidenData.getListData());
    }

    private void showRecyclerCardView() {
        itemCategory.setLayoutManager(new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
        ));
        cardViewAdapter = new CardViewAdapter(this, data);
        itemCategory.setAdapter(cardViewAdapter);
    }

    public void showRecyclerGrid(){
        //spanCount --> jumlah kolom tampilan
        itemCategory.setLayoutManager(new GridLayoutManager(this, 2));
        gridAdapter = new GridAdapter(this, data);
        itemCategory.setAdapter(gridAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()){
            case R.id.action_cardview:
                showRecyclerCardView();
                title = "RecyclerView - CardView";
                break;
            case R.id.action_grid:
                showRecyclerGrid();
                title = "RecyclerView - Grid";
                break;
        }
        getSupportActionBar().setTitle(title);
        return super.onOptionsItemSelected(item);
    }
}
