package com.example.xsisacademy.recycleview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.xsisacademy.recycleview.R;
import com.example.xsisacademy.recycleview.data.Presiden;

import java.util.ArrayList;

/**
 * Created by XsisAcademy on 24/06/2019.
 */

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.CardViewHolder>{
    private Context context;
    private ArrayList<Presiden> listPresiden;

    public CardViewAdapter(Context context, ArrayList<Presiden> listPresiden) {
        this.context = context;
        this.listPresiden = listPresiden;
    }

    @NonNull
    @Override
    public CardViewAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_presiden_cardview, parent,false);
        return new CardViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewAdapter.CardViewHolder holder, int position) {
        final Presiden p = listPresiden.get(position);
        Glide.with(context)
                .load(p.getImg())
                .into(holder.imgPhoto);
        holder.itemPeriod.setText(p.getPeriod());
        holder.itemName.setText(p.getName());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Detail "+ p.getName() + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPresiden.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView itemName, itemPeriod;
        Button btnDetail;
        public CardViewHolder(View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.imgPhoto);
            itemName = itemView.findViewById(R.id.itemName);
            itemPeriod = itemView.findViewById(R.id.itemPeriod);
            btnDetail = itemView.findViewById(R.id.btnDetail);
        }
    }
}
