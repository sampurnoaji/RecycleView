package com.example.xsisacademy.recycleview.data;

/**
 * Created by XsisAcademy on 24/06/2019.
 */

public class Presiden {
    private String name, period, img;

    public String getName() {
        return name;
    }

    public String getPeriod() {
        return period;
    }

    public String getImg() {
        return img;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
