package com.example.xsisacademy.recycleview.data;

import java.util.ArrayList;

/**
 * Created by XsisAcademy on 24/06/2019.
 */

public class PresidenData {
    public static String[][] data = new String[][]{
            {"Marshal D Teach", "2,247,600,000", "https://pm1.narvii.com/6938/968eb6c073161e19a7f5aac7d1d5540c6fb02df5r1-735-1059v2_hq.jpg"},
            {"Monkey D Luffy", "1,500,000,000", "https://pm1.narvii.com/6938/6f62617dd003d5bcb8094a9ed18fe71eaa5db416r1-554-857v2_hq.jpg"},
            {"'Surgeon of Death'\nTrafalgar D Water Law", "500,000,000", "https://pm1.narvii.com/6938/524867ee8fed66cd7a281fc7cd55d9d316a1746cr1-499-750v2_hq.jpg"},
            {"Eustass 'Captain' Kid", "470,000,000", "https://pm1.narvii.com/6938/bf29a6582e1b7c86e34354a7d1330af38571dd56r1-543-770v2_hq.jpg"},
            {"'Roar of The Sea'\nScratchmen Apoo", "350,000,000", "https://pm1.narvii.com/6938/6f6cb54e3f4e80f61eca40d407216ef030a07ea6r1-566-800v2_hq.jpg"},
            {"'Magician' Basil Hawkins", "320,000,000", "https://pm1.narvii.com/6938/be6cc3accb8f30358146eff47280997d42cc9945r1-480-640v2_hq.jpg"},
            {"'Pirate Hunter'\nRoronoa Zoro", "320,000,000", "https://pm1.narvii.com/6938/ed99928913aff2fd7fe8da643115237014514b57r1-480-800v2_hq.jpg"},
            {"Capone 'Gang' Bege", "300,000,000", "https://pm1.narvii.com/6938/17a01bca25cc241ac0ca478bf6e0a7d43ea770cer1-600-870v2_hq.jpg"},
            {"'Red Flag' X Drake", "222,000,000", "https://pm1.narvii.com/6938/16d6314fe589798e284681482f836fbafa085df7r1-451-627v2_hq.jpg"},
            {"'Massacre Soldier' Killer", "200,000,000", "https://pm1.narvii.com/6938/eae162754a032d94f44d6f0d60261a4524c238e4r1-600-979v2_hq.jpg"},
            {"'Big Eater' Jewelry Bonnie", "140,000,000", "https://pm1.narvii.com/6938/3c7775eea0260deb2c26fabac750ba9f9edeb553r1-236-325v2_hq.jpg"},
            {"'Mad Monk' Urouge", "108,000,000", "https://pm1.narvii.com/6938/a9cfc45ded678ed292a7d2dada023d7dbc19b232r1-566-800v2_hq.jpg"},
            {"'King' Firin\n'Manusia Terkuat'", "9,999,999,999", "https://i1.rgstatic.net/ii/profile.image/696013648064512-1542953993072_Q512/Muhamad_Mustaghfirin.jpg"}
    };

    public static ArrayList<Presiden> getListData(){
        Presiden p;
        ArrayList<Presiden> list = new ArrayList<>();
        for (int i=0; i<data.length; i++){
            p = new Presiden();
            p.setName(data[i][0]);
            p.setPeriod(data[i][1]);
            p.setImg(data[i][2]);

            list.add(p);
        }
        return list;
    }
}
